#!/usr/bin/env bash

# REQUIREMENTS
# ffmpeg - https://ffmpeg.org/

# EXAMPLES:
#
# audio_to_mp3.sh our_audio.ogg  # one file
#
# audio_to_mp3.sh *.ogg         # multiple files

files=($@)

for file in "${files[@]}"; do
    echo "Converting $file..."
    ffmpeg -i $file -n -v 1 -ar 44100 -ac 2 -b:a 192k ${file%.*}.mp3
    echo " "
done
