#!/usr/bin/env bash

# REQUIREMENTS
# ffmpeg - https://ffmpeg.org/

# EXAMPLE
# video_trim_by_sec.sh our_video.mp4 our_new_video.mp4 0:01:00 25

read -p "Input file [$1]: " input
input=${input:-$1}

read -p "Output file [$2]: " output
output=${output:-$2}

read -p "Start [0:00:00]: " start
start=${start:-0:00:00}

read -p "Seconds to trim [30]: " end
end=${end:-30}

echo "Trimming $end seconds of $input starting $start"
ffmpeg -i $input -ss $start -t 0:00:$end -c:v copy -c:a copy $output
