#!/usr/bin/env bash

# REQUIREMENTS
# ffmpeg - https://ffmpeg.org/

# EXAMPLE
# ./video_trim.sh our_vide0.mp4 trim_video.mp4 0:00:30 0:01:00

read -p "Input file [$1]: " input
input=${input:-$1}

read -p "Output file [$2]: " output
output=${output:-$2}

read -p "Start [0:00:00]: " start
start=${start:-0:00:00}

read -p "End [0:00:30]: " end
end=${end:-0:00:30}

echo "Trimming $input from $start to $end"

ffmpeg -i $input -ss $start -to $end -c:v copy -c:a copy $output
