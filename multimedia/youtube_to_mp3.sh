#!/usr/bin/env bash

# REQUIREMENTS
# yt-dlp - https://github.com/yt-dlp/yt-dlp
# ffmpeg - https://ffmpeg.org/

# Example
# ./youtube_to_mp3.sh https://example.com/video

read -p "Video URL [$1]: " input
input=${input:-$1}

yt-dlp --extract-audio --audio-format mp3 --write-thumbnail $input
