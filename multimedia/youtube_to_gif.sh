#!/usr/bin/env bash

# REQUIREMENTS
# yt-dlp - https://github.com/yt-dlp/yt-dlp
# ffmpeg - https://ffmpeg.org/

# Example
# ./youtube_to_gif.sh https://example.com/video our_video 1024x768

INPUT="$1"
OUTPUT=$2
MP4="$OUTPUT.mp4"
GIF="$OUTPUT.gif"
SIZE=$3
palette="/tmp/palette.png"
filters="fps=10,scale=$3:-1:flags=lanczos"

yt-dlp $INPUT -o $MP4

ffmpeg -i $MP4 -vf "$filters,palettegen" -y $palette
ffmpeg -i $MP4 -i $palette -lavfi "$filters [x]; [x][1:v] paletteuse" -y $GIF

rm $MP4
