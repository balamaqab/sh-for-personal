#!/usr/bin/env bash

# VIDEO4WHATSAPP
# v0.6.0-20210112

# REQUIREMENTS
# ffmpeg - https://ffmpeg.org/

# EXAMPLE
# video_resize original.mp4 resized.mp4 16MB 800x600

read -p "Input file [$1]: " input
input=${input:-$1}

read -p "Output file [$2]: " output
output=${output:-$2}

weightd=$3
weightd=${weightd:-16MB}
read -p "File size [$weightd]: " weight
weight=${weight:-$weightd}

sized=$4
sized=${sized:-768x432}
read -p "Resolution [$sized]: " size
size=${size:-$sized}

echo "===================="
echo ""
echo "Source: $input"
echo "Output: $output"
echo "File size: $weight"
echo "Resolution: $size"
echo ""
echo "ffmpeg -i $input -vcodec libx264 -acodec aac -fs $weight -s $size $output"
echo "===================="

if [[ -n $input ]] && [[ -n $output ]]; then
    ffmpeg -i $input -vcodec libx264 -acodec aac -fs $weight -s $size $output
else
    echo "Input and output params are required"
fi
echo "===================="
