#!/usr/bin/env bash

# Example:
# backup_files our_backup

DESTINY=${1:-"."}

# Directories to backup
DIRS=(
    "Documents"
    "Desktop"
    "Pictures"
)

# File patterns to exclude
EXCLUDE_LIST=(
    "node_modules"
    "vendor"
    ".DS_Store"
)

# Preparing excludes from backup
EXCLUDES=""
for e in "${EXCLUDE_LIST[@]}"; do
    EXCLUDES+=" --exclude=$e"
done

# Preparing includes from backup
INCLUDES=""
for e in "${DIRS[@]}"; do
    INCLUDES+=" $e"
done

# Copying files from selected directories to current directory
echo " "
echo "=============================="
echo "          BACKUP"
echo " "
for e in "${DIRS[@]}"; do
    echo " * $e"
done
echo "=============================="
sudo rsync -ah --safe-links --ignore-errors --delete --stats $EXCLUDES $INCLUDES $DESTINY
echo "========== THE END ==========="
echo " "
